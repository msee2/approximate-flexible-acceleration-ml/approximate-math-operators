/*
 * Copyright 2023
 * Author: Luis G. Leon Vega <luis.leon@ieee.org>
 */

#include "axc_nl.hpp"

void axc_nl_accel_top(const DataType a, DataType& b) { axc_nl(a, b); }
