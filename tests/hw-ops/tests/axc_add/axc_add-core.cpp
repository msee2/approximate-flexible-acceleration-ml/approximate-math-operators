/*
 * Copyright 2022
 * Author: David Cordero Chavarría <dacoch215@estudiantec.cr>
 */

#include "axc_add.hpp"

void axc_add_accel_top(DataType const a, DataType const b, DataType& c) {
  axc_add(a, b, c);
}
