############################################################
## Copyright 2022
## Author: Luis G. Leon-Vega <lleon95@estudiantec.cr>
############################################################

# Repositories
FAL_INCLUDES += -I$(abspath $(REPO_DIR)/axc-math)

export FAL_INCLUDES
